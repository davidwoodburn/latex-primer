import numpy as np
import tkz

R1 = 0.016 # lat-long radius (rad)
R2 = 0.018 # lat-long radius (rad)
theta = np.linspace(0, 2*np.pi, 36001)
lat1 = (R1/4)*np.sin(2*theta)
lon1 = R1*(np.cos(theta) - 1)
lat2 = (R2/3)*np.sin(theta) + (R2/6)*np.sin(3*theta)
lon2 = R2*(np.cos(theta) - 1) + (R2 - R1)

fig = tkz.graph("fig_tkz")
fig.plot(lon1, lat1, label="smaller")
fig.plot(lon2, lat2, label="larger")
fig.xlabel = "Longitude (rad)"
fig.ylabel = "Latitude (rad)"
fig.equal = True
fig.render()
