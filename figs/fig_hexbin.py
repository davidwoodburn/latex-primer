import numpy as np
import matplotlib.pyplot as plt
from matplotlib.collections import RegularPolyCollection
import tkz

np.random.seed(0)

K = 10000
x = 0.5 + 0.1*np.random.randn(K)
y = 0.5 + 0.1*np.random.randn(K)

if False:
    fig = tkz.graph("fig_hexbin")
    fig.hexbin(x, y, radius=-0.05, label="size")
    fig.hexbin(x + 0.5, y, radius=0.05, label="shade")
    fig.scatter(x + 1.0, y, radius=0.008, marker=tkz.DOTS, label="scatter")
    fig.width = 5*2.54
    fig.equal = True
    fig.xlabel = "$x$ axis"
    fig.ylabel = "$y$ axis"
    fig.render()
else:
    # Create the plot
    fig, ax = plt.subplots()

    # Calculate hexbin
    hb = ax.hexbin(x, y, gridsize=50, visible=False, label="size")

    # Get bin counts and centers
    counts = hb.get_array()
    centers = hb.get_offsets()

    # Remove empty bins
    mask = counts > 0
    counts = counts[mask]
    centers = centers[mask]

    # Calculate sizes based on counts
    sizes = counts * 0.2  # Adjust the multiplier to change overall size

    # Create RegularPolyCollection
    hexagon = RegularPolyCollection(
        numsides=6,  # hexagon
        sizes=sizes,
        offsets=centers,
        transOffset=ax.transData,
        edgecolors='none',
        facecolors='blue',
        linewidths=1
    )

    # Add collection to axis
    ax.add_collection(hexagon)

    # Set labels and title
    ax.set_xlabel("$x$ axis")
    ax.set_ylabel("$y$ axis")

    #plt.hexbin(x, y, cmap=plt.cm.Blues, mincnt=1, edgecolors=None,
    #           gridsize=50, reduce_C_function=np.sum, label="size")
    ax.hexbin(x + 0.5, y, cmap=plt.cm.Greens, mincnt=1, edgecolors=None,
               gridsize=50, label="shade")
    ax.scatter(x + 1.0, y, color="r", s=0.01, edgecolors=None, label="scatter")
    plt.axis("equal")
    ax.set_xlabel("$x$ axis")
    ax.set_ylabel("$y$ axis")
    plt.legend()
    plt.savefig("fig_hexbin_plt.pdf")
    plt.show()
