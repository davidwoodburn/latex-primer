"""
This code generates the random spread of symbols used for the cover art of the
book.
"""

import numpy as np
import matplotlib.pyplot as plt

m = 0.5
w = 7.12 - m
h = 10.93 - m
marks = ["$\\alpha$", "$A$", "$\\beta$", "$B$", "$\\gamma$", "$\\Gamma$",
         "$\\delta$", "$\\Delta$", "$\\epsilon$", "$E$", "$\\zeta$", "$Z$",
         "$\\eta$", "$H$", "$\\theta$", "$\\Theta$", "$\\iota$", "$I$",
         "$\\kappa$", "$K$", "$\\lambda$", "$\\Lambda$", "$\\mu$", "$M$", "$v$",
         "$N$", "$\\xi$", "$\\Xi$", "$o$", "$O$", "$\\pi$", "$\\Pi$", "$\\rho$",
         "$P$", "$\\sigma$", "$\\Sigma$", "$\\tau$", "$T$", "$\\upsilon$",
         "$\\Upsilon$", "$\\phi$", "$\\Phi$", "$\\chi$", "$X$", "$\\psi$",
         "$\\Psi$", "$\\omega$", "$\\Omega$"]
vik = ["A", "B", "D", "E", "F", "G", "H", "I", "K", "L", "M", "N", "O", "R",
       "S", "T", "U", "Y", "Z"]
imaj = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N",
        "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"]
sizes = ["", "\\large ", "\\Large ", "\\LARGE ", "\\huge ", "\\Huge "]
print(len(marks) + len(vik) + len(imaj))
N = len(marks)

# Define positions.
Nx = 8
Ny = 12
dx = 2*w/Nx
dy = 2*h/Ny
xx = np.zeros(Nx*Ny)
yy = np.zeros(Nx*Ny)
k = 0
for j in range(Ny):
    for i in range(Nx):
        xx[k] = -w + i*dx + (0.8*np.random.rand() + 0.1)*dx
        yy[k] = -h + j*dy + (0.8*np.random.rand() + 0.1)*dy
        k += 1
kk = np.arange(Nx*Ny)
np.random.shuffle(kk)
xx = xx[kk]
yy = yy[kk]

# Math symbols
for n in range(len(marks)):
    s = np.random.randint(0, 5)
    x = xx[n]
    y = yy[n]
    print("    \\node[gray, scale=1.5] at (%.2f,%.2f) {%s%s};" %
          (x, y, sizes[s], marks[n]))

# Viking symbols
print("    \\begin{scope}\n        \\ff{vik}")
for n in range(len(vik)):
    s = np.random.randint(0, 5)
    x = xx[n + len(marks)]
    y = yy[n + len(marks)]
    print("        \\node[gray, scale=1.5] at (%.2f,%.2f) {%s%s};" %
          (x, y, sizes[s], vik[n]))
print("    \\end{scope}")

# Imaj symbols
print("    \\begin{scope}\n        \\ff{imaj}")
for n in range(len(imaj)):
    s = np.random.randint(0, 5)
    x = xx[n + len(marks) + len(vik)]
    y = yy[n + len(marks) + len(vik)]
    print("        \\node[gray, scale=0.8] at (%.2f,%.2f) {%s%s};" %
          (x, y, sizes[s], imaj[n]))
print("    \\end{scope}")
